# Send AT Commands to a cellular modem

This utility sends AT commands to the MultiTech MTD-H5-2.0 cellular module.
Should work with other modems but this is the only one I've tested with.
It can send commands either from the command line or from a file (with one AT command
per line).


[![ISC License](https://img.shields.io/badge/license-ISC-blue.svg)](https://gitlab.com/michaeljohn/atcmd/blob/master/LICENSE)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/michaeljohn/atcmd)](https://goreportcard.com/report/gitlab.com/michaeljohn/atcmd)
[![GoDoc](https://godoc.org/gitlab.com/michaeljohn/atcmd?status.svg)](https://godoc.org/gitlab.com/michaeljohn/atcmd)

## To Build

```bash
go build
```

## To Run

Send commands from the command line.

```bash
./atcmd -device /dev/ttyACM0 -cmd AT+CIMI  #Returns International Mobile Subscriber Identity
./atcmd -device /dev/ttyACM0 -cmd AT+CGMR  #Returns software revision number
```

Send commands from a file (the extension of the file does not matter).

```bash
./atcmd -device /dev/ttyACM0 -file cmds.at
./atcmd -device /dev/ttyACM0 -file cmds.txt
```

### Command file format

    ATZ
    AT+CMEE=1
    AT#SIMDET?
    AT+CIMI
    AT+CGMR
    AT+CPIN


## Example

```bash
./atcmd -device /dev/ttyACM0 -cmd "AT#QSS?"

#QSS: 0,1

OK

./atcmd -device /dev/ttyACM0 -cmd "AT+CGMR"

12.00.024

OK

```
