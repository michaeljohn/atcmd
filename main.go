// See LICENSE file for copyright and license details.

// atcmd
// This utility sends AT commands to the MultiTech MTD-H5-2.0 cellular module.
// It can send commands either from the command line or from a file (with one AT command
// per line).
//
package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"syscall"
	"unsafe"

	"flag"
)

const (
	version string = "1.0.0"

	recvTimeout uint8 = 2               // Timout in deciseconds
	baud              = syscall.B115200 // baud rate
)

type serial struct {
	f *os.File
}

// Open serial port to the modem.
func openPort(modemDevice string) *serial {
	var err error
	s := new(serial)

	// Open serial port to the device
	s.f, err = os.OpenFile(modemDevice, syscall.O_RDWR|syscall.O_NOCTTY|syscall.O_NONBLOCK, 0666)
	if err != nil {
		fmt.Printf("Failed to open %s: %v\n", modemDevice, err)
		os.Exit(1)
	}
	// Termios settings
	t := syscall.Termios{
		Iflag:  syscall.IGNPAR,
		Cflag:  syscall.CS8 | syscall.CREAD | syscall.CLOCAL | baud,
		Cc:     [32]uint8{syscall.VMIN: 0, syscall.VTIME: recvTimeout},
		Ispeed: baud,
		Ospeed: baud,
	}
	// syscall to set our termios settings for our file handle
	syscall.Syscall6(syscall.SYS_IOCTL,
		uintptr(s.f.Fd()), uintptr(syscall.TCSETS), uintptr(unsafe.Pointer(&t)),
		0, 0, 0,
	)
	// Set nonblocking to false
	if err := syscall.SetNonblock(int(s.f.Fd()), false); err != nil {
		fmt.Printf("Failed to set nonblocking mode: %v\n", err)
		os.Exit(1)
	}

	return s
}

// Close serial port.
func (s *serial) Close() {
	s.f.Close()
}

// Write data to the serial port.
// Appends a carriage return to the end of str.
func (s *serial) Write(str string) error {
	nbytes, err := s.f.WriteString(str + "\r")
	if err != nil {
		return err
	}
	if nbytes != len(str)+1 {
		return errors.New("Looks like we failed to send the full message")
	}
	return nil
}

// Read reply from the serial port.
func (s *serial) Read() (string, error) {
	buf := make([]byte, 128)
	ret := new(bytes.Buffer)
	for {
		_, err := s.f.Read(buf)
		if err != nil && err != io.EOF {
			return "", err
		}
		if err == io.EOF {
			break
		}
		ret.Write(buf)
	}
	// remove null bytes
	ret2 := new(bytes.Buffer)
	for _, i := range ret.Bytes() {
		if i != 0x00 {
			ret2.WriteByte(i)
		}
	}
	return ret2.String(), nil
}

// SendCmd sends cmd to the modem and returns the reply.
func (s *serial) SendCmd(cmd string) (reply string, err error) {
	// Send the command
	if err = s.Write(cmd); err != nil {
		return "", fmt.Errorf("Failed to send AT command: %v", err)
	}

	// Receive the reply
	reply, err = s.Read()
	if err != nil {
		return "", fmt.Errorf("Failed to receive reply from modem: %v", err)
	}
	// Contains OK?
	//fmt.Println(bytes.Contains([]byte(reply), []byte{0x0d, 0x0a, 0x4f, 0x4b, 0x0d, 0x0a}))
	return reply, nil
}

func main() {
	var (
		optCmd     string
		optFile    string
		optDevice  string
		optVersion bool
	)

	flag.StringVar(&optCmd, "cmd", "", "AT command to execute")
	flag.StringVar(&optFile, "file", "", "File with AT command to execute")
	flag.StringVar(&optDevice, "device", "", "Device to use")
	flag.BoolVar(&optVersion, "version", false, "Print the version and exit")
	flag.Parse()

	if optVersion {
		fmt.Println(version)
		os.Exit(0)
	}

	s := openPort(optDevice)
	defer s.Close()

	switch {
	case len(optCmd) > 0:
		reply, err := s.SendCmd(optCmd)
		if err != nil {
			fmt.Printf("%v\n", err)
		} else {
			fmt.Printf("%v\n", reply)
		}
	case len(optFile) > 0:
		content, err := os.Open(optFile)
		if err != nil {
			fmt.Printf("%v\n", err)
			os.Exit(1)
		}
		defer content.Close()
		scanner := bufio.NewScanner(content)
		for scanner.Scan() {
			fmt.Printf(">> %v\n", scanner.Text())
			reply, err := s.SendCmd(scanner.Text())
			if err != nil {
				fmt.Printf("%v\n", err)
			} else {
				fmt.Printf("%v\n", reply)
			}
		}
	default:
		fmt.Println("No recognized option given.")
		fmt.Printf("Run: %s --help\n", os.Args[0])
		os.Exit(1)

	}
}
